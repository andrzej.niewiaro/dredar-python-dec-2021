print('Exercise 1:')
print('Dog')

favourite_car = 'Skoda Scala'
age = 18

print(favourite_car, age, sep='\n')

print('Exercise 2:')
print('    *', '   * *', '  *   *', ' *     *', '  *   *', '  *   *', '  * * *', sep='\n')

print("""
     *
   *  *
  *    *
 *      *
*        *
***    ***
  *    *
  *    *
  ******
""")

print('Exercise 3:')
x = 'Cześć'
y = 'wszystkim!'
print(x, y)

print('Exercise 4:')
a = 3.0
b = 4.0
c = (2 + a * 2 + b * 2) * 0.5
print(c)

print('Exercise 5:')
kilometers = 12.25
miles = kilometers / 1.6
print(kilometers, "kilometers is equal to", miles, "miles")
