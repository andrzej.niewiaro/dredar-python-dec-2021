print('EXERCISE 1\n')
print('Dog')
print('\n')
print('EXERCISE 2\n')
favorite_car = 'Skoda Scala'
age = 18
print(favorite_car, age, sep='\n')
print('\n')
print('EXERCISE 3\n')
print('     *', '   *  *', '  *    *', ' *      *', '*        *', '***    ***', '  *    *', '  *    *', '  *****',
      sep='\n'
      )
print('\n')
print('EXERCISE 4\n')
x = "Cześć"
y = "Wszystkim!"
print(x, y)
print('\n')
print('EXERCISE 5\n')
a = 3.0
b = 4.0

c = (2 + a * 2 + b * 2) * 0.5

print(c)
print('\n')
print('EXERCISE 6\n')
kilometers = 12.25
miles = kilometers * 0.62137
print(kilometers, "kilometers is equal to", miles, "miles")
