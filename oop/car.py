class Car:
    def __init__(self, brand):
        self.brand = brand
        self.age = 0
        self.total_distance = 0
        self.amount_of_gas = 0

    def add_gas_amount(self, new_amount):
        self.amount_of_gas += new_amount

    def drive(driven_km):
        self.total_distance += driven_km
        self.amount_of_gas -= 10