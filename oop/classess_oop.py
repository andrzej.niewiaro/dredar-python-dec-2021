from oop.animals import Dog


dog1 = Dog('Lessie')
dog2 = Dog('Chika')

print(dog1.name)
print(dog2.name)

dog1.name = 'Azor'
dog2.name = 'Marshal'

print(dog1.name)
print(dog2.name)

dog2.kind = 'rottweiler'

dog1.bark()

dog1.print_info()
dog2.print_info()