class Dog:
    def __init__(self, name):
        self.name = name
        self.kind = "German shepherd"

    def bark(self):
        print('Woof! woof!')

    def print_info(self):
        print(f'Name of dog: {self.name} Kind of dog: {self.kind}')
