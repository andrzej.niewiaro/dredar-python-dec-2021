class Player:
    def __init__(self, name, position, age):
        self.name = name
        self.age = age
        self.goals = 0
        self.position = position
        self.club = None
        self.cards = None
        self.field_number = None


    def score(self):
        if self.club:
            self.goals += 1
        else:
            print('choose your club')

    def change_position(self, new_position):
        self.position = new_position
        self.age += 2

    def change_club(self, new_club):
        self.club = new_club
        self.goals = 0

if __name__ == '__main__':
    my_player = Player('Test Player')
    print(my_player.age)
    my_player.age = 23
    print(my_player.age)
    print(my_player_goals)
    my_player.score()
    my_player.score()
    print(my_player.goals)