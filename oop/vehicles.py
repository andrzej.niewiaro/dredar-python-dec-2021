class Car:
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model
        self.total_distance = 0
        self.gas_amount = 5

    def drive(self,distance):
        self.total_distance += distance

car1 = Car('Toyota', 'Corolla')
car2 = Car('Mazda', 'MX5')
print(car1.total_distance)

car1.drive(500)
car2.drive(1000)
print(car1.total_distance)
print(car2.total_distance)

#  main   ctrl+J   if __name__ == '__main__':

"""
- brand (wym przy inicjalizacji)
- model (wymagane przy inicjalizacji)
- total distance (niewymagane przy inicjalizacji, pocz. wart 0)
- gas_amount (niewymagane przy inicjalizacji, pocz. wart: 5)

Utworz samochod "Toyota Corolla"
Wydrukuj jego total distance
"""