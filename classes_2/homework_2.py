print(' ', 'EXERCISE 1\n', sep='\n')
first_name = 'Andrzej'
last_name = 'Niewiarowski'
email = "ndrew@poczta.fm"
phone_number = '+4800000000'
age = 36
full_bio = f'My name is {first_name} {last_name}\nI\'m {age} years old.\nYou can contact me via email {email} or telephone {phone_number}'
print(full_bio)
print(' ', 'EXERCISE 2\n', sep='\n')
pi = 3.1415
r = 2
circumference = 2 * pi * r
area = pi * r ** 2
print('Circle of radius', r, 'has circumference equal to', circumference, 'and area of', area)

print(' ', 'EXERCISE 3\n', sep='\n')


def triangle_hypotenuse(a, b):
    import math
    return math.sqrt(a ** 2 + b ** 2)


def triangle_circumference(a, b, c):
    return a + b + c


def triangle_area(a, b):
    return (a * b) / 2


print('triangle hypotenuse:', triangle_hypotenuse(5, 13))

print('triangle circumference:', triangle_circumference(1, 2, 3))

print('triangle area:' ,triangle_area(6, 8))

print(' ', 'EXERCISE 4\n', sep='\n')


def can_use_promotion(aged, sex):
    is_senior = aged > 65
    is_woman = sex == 'f' and (aged <= 16 or aged > 45)
    is_male = sex == 'm' and 20 <= aged <= 40
    return is_senior or is_woman or is_male


print('Can 14-year-old boy use the promotion?', can_use_promotion(14, 'm'))
