pet = {
    'name': 'Genowefa',
    'age': 15,
    'type': 'Tortoise',
    'male': False
}

print(pet['name'])
pet['age'] = 16
print(pet['age'])
pet['owner'] = 'Zofia'

print(pet)

friend = {
    'name': "Michal",
    'age': 35,
    'hobbies': ['zoology', 'medicine']
}
print(friend['hobbies'][-1])

friend['hobbies'].append('astronomy')
print(friend)
