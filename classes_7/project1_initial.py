countries_map = {
    'Japan': ('Toyota', 'Suzuki', 'Subaru'),
    'Germany': ('BMW', 'Audi', 'Volkswagen'),
    'South Korea': ('Hyundai',)
}
original_cars = [
    {'brand': 'Toyota', 'year': 1998, 'name': 'Camry'},
    {'brand': 'BMW', 'year': 2008, 'name': '330'},
    {'brand': 'Audi', 'year': 2020, 'name': 'RS3'},
    {'brand': 'Hyundai', 'year': 2017, 'name': 'i30'},
]

def get_name_of_car(car_dictionary):
    brand = car_dictionary['brand']
    name = car_dictionary['name']
    return f'{brand} {name}'
    # Fill the function to return model of a car as a string containing brand and model
    # Input argument is single car dictiqonary

print(get_name_of_car(original_cars[-1]))


def get_age_of_car(car_dictionary):
    car_year = car_dictionary['year']
    car_age = 2021 - car_year
    return int(car_age)
# Fill the function to return age of a car
# Input argument is single car dictionary
print(get_age_of_car(original_cars[0]))


def get_country_of_car(car_dictionary):
    brand = car_dictionary['brand']
    for country, brands in countries_map.items():
        if brand in brands:
            return country


def convert_car(car_dictionary):
    return {
        'name': get_name_of_car(car_dictionary),
        'country': get_country_of_car(car_dictionary),
        'age': get_age_of_car(car_dictionary)
    }
convert_car(original_cars[0])


def convert_cars_data(cars_list):
    converted_cars = []
    for car in cars_list:
        # For each car in the list use function for converting car
        # And using it add converted car to converted_cars list
        converted_car = convert_car(car)
        converted_cars.append(converted_car)
    print(converted_cars)

print(convert_cars_data(original_cars[-1]))


"""
Use convert_cars_data on the list above and print new list that should look like this:
[
    {'model': 'Toyota Camry', 'country': 'Japan', 'age': 23}, 
    {'model': 'BMW 330', 'country': 'Germany', 'age': 13}, 
    {'model': 'Audi RS3', 'country': 'Germany', 'age': 1}, 
    {'model': 'Hyundai i30', 'country': 'South Korea', 'age': 4}
]
"""
