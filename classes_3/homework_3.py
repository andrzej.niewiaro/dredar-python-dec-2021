print('EXERCISE 1', sep='\n')
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]

number_of_primes = len(primes)

print('prime numer:', number_of_primes, "\n")

primes.append(666)

print("appended:", primes, "\n")
first_four = primes[0:4]
print("first four from list", first_four, "\n")

sum_of_elements = sum(primes)
number_of_elements = len(primes)
average_of_elements = round(sum_of_elements / number_of_elements, 1)
print(f'average of elements {average_of_elements}\n')


print('EXERCISE 2', sep='\n')

random_numbers = [22, 3.3, -2, 5, 701, 42, -120, 35, 69.9, 123, -444, 0.0, 0]


def sortlist(users_list):
    result = sorted(users_list)
    return result


def show_last_three_list_elements(new_list):
    result = new_list[-3:]
    return result

def show_first_list_element(my_list):
    result = my_list[0]
    return result

def show_first_three_elements(some_list):
    result = some_list[0:3]
    return result

print(f'Unsorted list: {random_numbers}\n')

print(f'Sorted list: {sortlist(random_numbers)}\n')

print(f'last three items {show_last_three_list_elements(random_numbers)}\n')

print(f'first item {show_first_list_element(random_numbers)}\n')

print(f'first three items {show_first_three_elements(random_numbers)}\n')

print('EXERCISE 3', sep='\n')


def multiply(a, b, c):
    return a * b * c


multiplication_example1 = multiply(1, 2, 3)

multiplication_example2 = multiply(5, 11, 4)

print("results of multiplication:", multiplication_example1, multiplication_example2, "\n")

print('EXERCISE 4', sep='\n')


def can_buy_booze(age):
    is_allowed_to_buy_booze = age >= 18
    return is_allowed_to_buy_booze


little_boy_age = can_buy_booze(14)

print(f'is 14-year old Kamil allowed to buy booze?\n{little_boy_age}\n')

