movies = ['Terminator', 'Predator', 'Popiol i Diament', 'Matrix', 'Interstellar']
first_movie = movies[0]

print(f'first movie: {first_movie}\n')
print('thrid movie:', movies[2], '\n')
#funkcja
number_of_movies = len(movies)
print('number of movies:', number_of_movies,'\n')
last_movie_long_way = [number_of_movies-1]
last_movie = movies[-1]

print('last movie:', last_movie, '\n')
#metoda na obiekcie
movies.append('Star Wars')

print('last movie of appended list:', movies[-1], '\n')
print('number of movies NOW:', len(movies))