all_data = [['Income', 'Cost', 'Tax', 'Profit'], [200000, 50000, 15000, 135000], [90000, 45000, 20000, 17000],
            [900000, 20000, 320000, 560000]]

import pandas as pd

df = pd.DataFrame(all_data)
df.to_csv('file2.csv', index=False, header=False)
