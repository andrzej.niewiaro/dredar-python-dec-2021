from oop.football import Player

def test_initial_values_for_player():
    player = Player('test_name', 'goalkeeper', 67)
    assert player.name == 'test_name'
    assert player.position == 'goalkeeper'
    assert player.age == 67
    assert player.club is None
    assert player.cards == 0
    assert player.field_number == 0
    assert player.goals == 0

def test_score_without_club_should_not_increase_goals():
    player = Player('test_name', 'goalkeeper', 67)
    player.score()
    assert player.goals == 0

def test_score_with_club_should_increase_goals():
    player = Player('test_name', 'goalkeeper', 67)
    player.club = 'Juventus'
    player.score()
    assert player.goals == 1

def test_changing_position_should_increase_age():
    player = Player('test_name', 'goakeeper', 67)
    player.change_position('defender')
    assert player.age == 35
    assert player.position == 'defender'

def test_changing_club_should_reset_goals():
    player = Player('test_name', 'goakeeper', 67)
    player.score()
    player.club = ('FC Barcelona')
    player.change_club('Real Madryt')
    assert player.goals == 0
    assert player.club == 'Real Madryt'
